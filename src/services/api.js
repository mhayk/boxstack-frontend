import axios from "axios";

const api = axios.create({
  baseURL: "https://boxstack-backend.herokuapp.com",
  headers: {'Cache-Control': 'no-cache'},
});

export default api;
